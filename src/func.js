const getSum = (str1, str2) => {
  if (typeof str1 !=='string' || typeof str2 !=='string' || isNaN(Number(str1)) || isNaN(Number(str2))){
    return false;
  } else {
    return (Number(str1) + Number(str2)).toString();
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numberOfPost = 0;
  let numberOfComments = 0;
  listOfPosts.forEach(el => {
    if(el.author === authorName){
      numberOfPost += 1;
    }
    el.comments?.forEach(value => {
      if (value.author === authorName){
        numberOfComments += 1;
      }
    })
  })
  return `Post:${numberOfPost},comments:${numberOfComments}`
};

const tickets=(people) => {
  let sum = 0;
  for (let i=0; i < people.length; i++){
    let person = Number(people[i]);
    let change = person - 25;
    if (sum >= change){
      sum+= 25;
    } else {
      return 'NO';
    }
  }
  return 'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
